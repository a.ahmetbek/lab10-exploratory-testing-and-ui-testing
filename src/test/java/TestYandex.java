import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;

public class TestYandex {

    @Test
    public void testYandex() {
        System.setProperty("webdriver.gecko.driver", "/home/ali/Desktop/geckodriver");
        WebDriver driver = new FirefoxDriver();
        driver.get("http://www.ya.ru");
        Assert.assertEquals("Яндекс", driver.getTitle());
        WebElement findLine = driver.findElement(By.xpath("/html/body/table/tbody/tr[2]/td/form/div[1]/span/span/input"));
        findLine.sendKeys("SQR course is the best");
        WebElement searchButton = driver.findElement(By.xpath("/html/body/table/tbody/tr[2]/td/form/div[2]/button"));
        searchButton.click();
        findLine = driver.findElement(By.xpath("/html/body/header/div[1]/div[1]/div[3]/form/div[1]/span/span/input"));
        Assert.assertEquals("SQR course is the best", findLine.getAttribute("value"));
        driver.quit();
    }

    @Test
    public void testImagesNavigation() throws InterruptedException {
        System.setProperty("webdriver.gecko.driver", "/home/ali/Desktop/geckodriver");
        WebDriver driver = new FirefoxDriver();
        driver.get("http://www.ya.ru");
        Assert.assertEquals("Яндекс", driver.getTitle());
        WebElement findLine = driver.findElement(By.xpath("/html/body/table/tbody/tr[2]/td/form/div[1]/span/span/input"));
        findLine.sendKeys("Innopolis");
        WebElement searchButton = driver.findElement(By.xpath("/html/body/table/tbody/tr[2]/td/form/div[2]/button"));
        searchButton.click();

        findLine = driver.findElement(By.xpath("/html/body/div[2]/nav/ul/li[2]/div/a"));
        findLine.click();
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        Assert.assertEquals(tabs.size(), 2);
        driver.switchTo().window(tabs.get(1));
        Thread.sleep(2000);
        Assert.assertEquals(driver.getCurrentUrl(), "https://yandex.ru/images/search?from=tabbar&text=Innopolis");
        driver.quit();
    }

    @Test
    public void testLogIn() {
        System.setProperty("webdriver.gecko.driver", "/home/ali/Desktop/geckodriver");
        WebDriver driver = new FirefoxDriver();
        driver.get("http://www.ya.ru");
        Assert.assertEquals("Яндекс", driver.getTitle());
        WebElement findLine2 = driver.findElement(By.xpath("/html/body/table/tbody/tr[1]/td/div/a"));
        findLine2.click();
        String url = driver.getCurrentUrl();
        Assert.assertEquals("https://mail.yandex.ru/", url);
        driver.quit();
    }
}
